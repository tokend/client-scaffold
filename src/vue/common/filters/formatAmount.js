import { i18n } from '../../../js/i18n'

export function formatAmount (value) {
  return i18n.c(value)
}
