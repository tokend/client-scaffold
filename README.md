#

> Tokend client scaffold

# Instructions for working with the project

1. Install node.js;
2. Install git
3. Clone a repository: ```git clone https://gitlab.com/tokend/client-scaffold.git```
4. In folder with project, execute: ``` npm install```
5. Usually, you need to execute ```npm install``` only once
6. Execute ```npm run init``` to configure project settings
7. To start project on local server, execute: ```npm run dev```
8. Open http://localhost:8095 in browser;
9. To stop local server, press `Ctrl + C` in terminal.
10. If the remote repository of the project was updated, you need to execute ```git pull``` command on your local machine to get these updates
11. After updating your repository (step 9), you need to execute steps 5 and 6 one more time to launch the updated project.

Node.js: https://nodejs.org/en/
Git: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
Project link: https://gitlab.com/tokend/client-scaffold


## Build Setup

``` bash
# install dependencies
npm install

# init initial project settings (git hooks)
npm run init

# serve with hot reload at localhost:8095
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```
